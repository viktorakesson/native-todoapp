import React, { useState } from "react";
import { Pressable, StyleSheet, Text, TextInput, View } from "react-native";

export const Input = ({ navigation, setTodos }) => {
  const [todo, setTodo] = useState("");

  const handlePress = () => {
    if (todo) {
      fetch("http://10.0.2.2:8080/todos", {
        method: "POST",
        body: JSON.stringify({
          title: todo,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      })
        .then(() => {
          fetch("http://10.0.2.2:8080/todos")
            .then((res) => res.json())
            .then((res) => setTodos(res));
        })
        .catch((err) => console.log(err));

      setTodo("");
    }
  };

  return (
    <View style={styles.inputcontainer}>
      <TextInput
        placeholder="Enter Todo"
        style={styles.input}
        value={todo}
        onChangeText={setTodo}
      />
      <Pressable styles={styles.button} onPress={handlePress}>
        <Text style={styles.text}>Add</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  inputcontainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    marginBottom: 30,
  },
  input: {
    height: 40,
    borderWidth: 1,
    padding: 10,
    width: "70%",
    color: "black",
  },
  button: {},
  text: {
    backgroundColor: "coral",
    height: 40,
    textAlignVertical: "center",
    paddingHorizontal: 15,
    borderRadius: 8,
    color: "white",
  },
});
