import React, { useState } from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";
import { Fontisto } from "@expo/vector-icons";

export const Todo = ({ item }) => {
  const [toggle, setToggle] = useState(item.completed);

  const toggleComplete = (id) => {
    fetch("http://10.0.2.2:8080/todos/" + id, { method: "PUT" })
      .then(setToggle(!toggle))
      .catch((err) => console.log(err));
  };

  return (
    <View style={styles.card}>
      <View>
        <Text>{item.todo}</Text>
      </View>
      <View>
        <Pressable onPress={() => toggleComplete(item.id)}>
          <Text>
            {toggle ? (
              <Fontisto name="checkbox-active" size={18} color="black" />
            ) : (
              <Fontisto name="checkbox-passive" size={18} color="black" />
            )}
          </Text>
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    flexDirection: "row",
    justifyContent: "space-between",
    shadowColor: "black",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 10,
    elevation: 3,
    backgroundColor: "white",
    padding: 15,
    margin: 10,
    borderRadius: 10,
  },
});
