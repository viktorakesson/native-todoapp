import React, { useEffect } from "react";
import { FlatList, Pressable, StyleSheet, View } from "react-native";
import { Todo } from "./Todo";

export const List = ({ todos, navigation, setTodos }) => {
  const handlePress = (item) => {
    fetch("http://10.0.2.2:8080/todos/" + item.id, { method: "GET" })
      .then((res) => res.json())
      .then((res) => navigation.navigate("Info", { item: res }))
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      fetch("http://10.0.2.2:8080/todos")
        .then((res) => res.json())
        .then((res) => setTodos(res))
        .catch((err) => console.log(err));
    });

    return unsubscribe;
  }, [navigation]);

  const renderItem = ({ item }) => {
    return (
      <View>
        <Pressable onPress={() => handlePress(item)}>
          <Todo item={item} />
        </Pressable>
      </View>
    );
  };

  return (
    <FlatList
      data={todos}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
    />
  );
};

const styles = StyleSheet.create({});
