import React from "react";
import { StyleSheet, Text } from "react-native";

export const Header = () => {
  return <Text style={styles.header}>Todo app...</Text>;
};

const styles = StyleSheet.create({
  header: {
    color: "black",
    fontSize: 30,
    textAlign: "center",
    margin: 30,
  },
});
