import React from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";

export const Info = ({ route, navigation }) => {
  const todo = route.params.item;

  const deleteTodo = (id) => {
    fetch("http://10.0.2.2:8080/todos/" + id, { method: "DELETE" })
      .then(() => navigation.navigate("Home"))
      .catch((err) => console.log(err));
  };

  return (
    <View style={styles.infocontainer}>
      <Text style={styles.header}>Todo details</Text>
      <Text style={styles.json}>{JSON.stringify(todo, null, 2)}</Text>
      <View style={styles.buttoncontainer}>
        <Pressable onPress={() => deleteTodo(todo.id)}>
          <Text style={styles.text}>Delete</Text>
        </Pressable>
        <Pressable onPress={() => navigation.popToTop()}>
          <Text style={styles.text}>Go back</Text>
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  infocontainer: {
    justifyContent: "center",
  },
  buttoncontainer: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  json: {
    marginLeft: "30%",
    marginVertical: 30,
  },
  text: {
    backgroundColor: "coral",
    height: 40,
    textAlignVertical: "center",
    textAlign: "center",
    paddingHorizontal: 15,
    borderRadius: 8,
    color: "white",
    width: 100,
    margin: 10,
  },
  header: {
    color: "black",
    fontSize: 30,
    textAlign: "center",
    margin: 30,
  },
});
