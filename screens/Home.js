import React, { useEffect, useState } from "react";
import { ImageBackground, StyleSheet, View } from "react-native";
import { Header } from "../components/Header";
import { Input } from "../components/Input";
import { List } from "../components/List";

export const Home = ({ navigation }) => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    fetch("http://10.0.2.2:8080/todos")
      .then((res) => res.json())
      .then((res) => setTodos(res))
      .catch((err) => console.log(err));
  }, []);

  return (
    <ImageBackground
      source={require("../assets/bg4.jpg")}
      resizeMode="cover"
      style={styles.image}
    >
      <View>
        <Header />
        <Input navigation={navigation} setTodos={setTodos} />
        <List todos={todos} navigation={navigation} setTodos={setTodos} />
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  image: {
    height: "100%",
  },
});
